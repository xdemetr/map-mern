const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PointSchema = new Schema ({
  name: {
    type: String,
    required: true
  },
  coordinates: {
    type: String,
    required: true
  },
  order: {
    type: Number,
    required: true
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  }
});

module.exports = Point = mongoose.model('points', PointSchema);
