import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import MapService from './services/map-service';
import { MapServiceProvider } from './components/map-service-context';
import store from './store';
import App from './components/app';

import jwtDecode from 'jwt-decode';
import setAuthToken from './utils/set-auth-token';

import { logoutUser, userLoaded } from './actions/auth-actions';

const mapService = new MapService();


if (localStorage.jwtToken) {
  setAuthToken(localStorage.jwtToken);
  
  const decoded = jwtDecode(localStorage.jwtToken);
  store.dispatch(userLoaded(decoded));
  
  const currentTime = Date.now() / 1000;
  
  if (decoded.exp < currentTime) {
    store.dispatch(logoutUser());
    window.location.href = '/';
  }
  
}

ReactDOM.render(
    <Provider store={store}>
      <MapServiceProvider value={mapService}>
        <Router>
          <App/>
        </Router>
      </MapServiceProvider>
    </Provider>,
    document.getElementById('root'));
