import setAuthToken from '../utils/set-auth-token';
import jwtDecode from 'jwt-decode';

import {
  POST_USER_REQUEST,
  POST_USER_SUCCESS,
  POST_USER_FAILURE,
  SET_MAP_CENTER
} from './types';

const userRequested = () => {
  return {
    type: POST_USER_REQUEST
  }
};

const userLoaded = (decoded) => {
  return {
    type: POST_USER_SUCCESS,
    payload: decoded
  }
};

const userError = (errors) => {
  return {
    type: POST_USER_FAILURE,
    payload: errors
  }
};

const mapCenterLoaded = (coordinates) => {
  return {
    type: SET_MAP_CENTER,
    payload: coordinates
  }
};

const logoutUser = () => dispatch => {
  localStorage.removeItem('jwtToken');
  setAuthToken(false);
  dispatch(userLoaded({}));
};

const registerUser = (mapSevice) => (userData, history) => dispatch => {
  dispatch(userRequested());
  
  mapSevice.userRegister(userData)
  .then(() => history.push('/login'))
  .catch(err => dispatch(userError(err.response.data)));
};

const loginUser = (mapService) => userData => dispatch => {
  dispatch(userRequested());
  
  mapService.userLogin(userData)
  .then((data) => {
    const { token } = data;
    
    localStorage.setItem('jwtToken', token);
    setAuthToken(token);
    
    const decoded = jwtDecode(token);
    dispatch(userLoaded(decoded));
  })
  .catch(err => dispatch(userError(err.response.data)));
};

const setMapCenter = (mapService) => coordinates => dispatch => {
  mapService.setMapCenter(coordinates)
  .then(() => dispatch(mapCenterLoaded(coordinates)))
  .catch(err => console.log(err))
};

export {
  registerUser,
  loginUser,
  logoutUser,
  setMapCenter,
  userLoaded
}
