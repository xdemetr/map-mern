import {
    FETCH_POINTS_REQUEST,
    FETCH_POINTS_SUCCESS,
    FETCH_POINTS_FAILURE,
    CLEAR_POINTS
} from './types';

const pointsLoaded = (points) => {
  return {
    type: FETCH_POINTS_SUCCESS,
    payload: points
  }
};

const pointsRequested = () => {
  return {
    type: FETCH_POINTS_REQUEST
  }
};

const pointsError = (errors) => {
  return {
    type: FETCH_POINTS_FAILURE,
    payload: errors
  }
};

const fetchPoints = (mapService) => userId => dispatch => {
  dispatch(pointsRequested());
  mapService.getPoints(userId)
  .then((data) => dispatch(pointsLoaded(data)))
  .catch((err) => dispatch(pointsError(err)));
};

const addPoint = (mapService) => pointData => dispatch => {
  dispatch(pointsRequested());
  
  mapService.addPoint(pointData)
  .then((data) => dispatch(pointsLoaded(data)))
  .catch(err => dispatch(pointsError(err.response.data)))
};

const updatePoint = (mapService) => (id, pointData) => dispatch => {
  dispatch(pointsRequested());
  
  mapService.updatePoint(id, pointData)
  .then((data) => dispatch(pointsLoaded(data)))
  .catch((err) => dispatch(pointsError(err.response.data)));
};

const sortPoints = (mapService) => points => dispatch => {
  dispatch(pointsRequested());
  
  mapService.sortPoints(points)
  .then((data) => dispatch(pointsLoaded(data)))
  .catch((err) => dispatch(pointsError(err.response.data)));
};

const deletePoint = (mapService) => pointId => dispatch => {
  dispatch(pointsRequested());
  
  mapService.deletePoint(pointId)
  .then((data) => dispatch(pointsLoaded(data)))
  .catch(err => dispatch(pointsError(err.response.data)))
};

const clearPoints = () => {
  return {
    type: CLEAR_POINTS
  }
};

export {
  fetchPoints,
  addPoint,
  updatePoint,
  sortPoints,
  deletePoint,
  clearPoints
}
