import {
  FETCH_POINTS_REQUEST,
  FETCH_POINTS_SUCCESS,
  FETCH_POINTS_FAILURE,
  CLEAR_POINTS
} from '../actions/types';

const pointReducer = (state, action) => {
  if (state === undefined) {
    return {
      points: {},
      loading: false
    }
  }
  
  switch (action.type) {
    case FETCH_POINTS_REQUEST:
      return {
        ...state.pointList,
        loading: true
      };
    
    case FETCH_POINTS_SUCCESS:
      return {
        points: action.payload,
        loading: false
      };
      
    case FETCH_POINTS_FAILURE:
      return {
        ...state.pointList,
        errors: action.payload,
        loading: false
      };
      
    case CLEAR_POINTS:
      return {
        points: {},
        loading: false
      };
    
    default:
      return state.pointList;
  }
};

export default pointReducer;
