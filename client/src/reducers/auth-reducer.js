import {
  SET_MAP_CENTER,
  POST_USER_REQUEST,
  POST_USER_SUCCESS,
  POST_USER_FAILURE
} from '../actions/types';
import isEmpty from '../validation/is-empty';

const authReducer = (state, action) => {
  if (state === undefined) {
    return {
      isAuthenticated: false,
      user: {},
      loading: false,
      errors: {}
    }
  }

  switch (action.type) {
  
    case POST_USER_REQUEST:
      return {
        isAuthenticated: !isEmpty(action.payload),
        user: action.payload,
        loading: true
      };
    
    case POST_USER_SUCCESS:
      return {
        isAuthenticated: !isEmpty(action.payload),
        user: action.payload,
        loading: false
      };
      
    case POST_USER_FAILURE:
      return {
        ...state.auth,
        errors: action.payload,
        loading: false
      };
  
    case SET_MAP_CENTER:
      return {
        ...state.auth,
        user: {
          ...state.user,
          mapCenter: action.payload
        },
        loading: false
      };

    default:
      return state.auth;
  }
};

export default authReducer;
