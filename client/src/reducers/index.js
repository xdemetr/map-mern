import authReducer from './auth-reducer';
import pointReducer from './point-reducer';

const reducer = (state, action) => {
  return {
    auth: authReducer(state, action),
    pointList: pointReducer(state, action)
  }
};

export default reducer;
