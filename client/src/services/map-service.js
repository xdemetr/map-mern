import axios from 'axios';

export default class MapService {
  
  userLogin(userData) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        axios.post('/api/users/login', userData)
        .then(res => resolve(res.data))
        .catch(err => reject(err));
      }, 500);
    })
  }
  
  userRegister(userData) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        axios.post('/api/users/register', userData)
        .then(res => resolve(res.data))
        .catch(err => reject(err));
      }, 500);
    })
  }
  
  getPoints(userData) {
    return new Promise((resolve, reject) => {
      axios.get('/api/points', userData)
      .then(res => resolve(res.data))
      .catch(err => reject(err));
    });
  }
  
  addPoint(pointData) {
    return new Promise((resolve, reject) => {
      axios.post('/api/points', pointData)
      .then(res => resolve(res.data))
      .catch(err => reject(err));
    });
  }
  
  deletePoint(pointId) {
    return new Promise((resolve, reject) => {
      axios.delete(`/api/points/${pointId}`)
      .then(res => resolve(res.data))
      .catch(err => reject(err));
    });
  }
  
  sortPoints(points) {
    return new Promise((resolve, reject) => {
      axios.put('/api/points', points)
      .then(res => resolve(res.data))
      .catch(err => reject(err));
    });
  }
  
  updatePoint(id, pointData) {
    return new Promise((resolve, reject) => {
      axios.put(`api/points/${id}`, pointData)
      .then(res => resolve(res.data))
      .catch(err => reject(err));
    });
  }
  
  setMapCenter(coordinates) {
    return new Promise((resolve, reject) => {
      axios.put(`api/users`, coordinates)
      .then(res => resolve(res.data))
      .catch(err => reject(err));
    });
  }
}
