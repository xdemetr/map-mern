import React from 'react';
import propTypes from 'prop-types';
import classnames from 'classnames';

const TextField = ({
  name,
  placeholder,
  value,
  error,
  type,
  onChange,
  autocomplete
}) => {
  return (
      <div className="form__item">
        <input
            type={type}
            className={ classnames('form__input', {'form__input_invalid': error}) }
            placeholder={placeholder}
            name={name}
            value={value}
            onChange={onChange}
            autoComplete={autocomplete}
        />
        {error && (<div className="form__error-notice">{error}</div> )}
      </div>
  );
};

TextField.propTypes = {
  name: propTypes.string.isRequired,
  value: propTypes.string.isRequired,
  type: propTypes.string.isRequired,
  onChange: propTypes.func.isRequired,
  placeholder: propTypes.string,
  error: propTypes.string
};

TextField.defaultProps = {
  type: 'text'
};

export default TextField;
