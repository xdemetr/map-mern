import React from 'react';

import './spinner.sass';

const Spinner = () => {
  return(
      <div className="spinner">
        <div className="lds-css ng-scope">
          <div className="lds-dual-ring">
            <div></div>
          </div>
        </div>
      </div>
  )
};

export default Spinner;
