import React, {Component} from 'react';
import propTypes from 'prop-types';
import { connect } from "react-redux";
import { addPoint } from '../../actions/point-actions';
import { bindActionCreators, compose } from 'redux'
import { withMapService } from "../hoc-helpers";
import AddForm from './add-form';

class AddFormContainer extends Component {
  state = {
    name: '',
    coordinates: '',
    errors: {}
  };
  
  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors,
        coordinates: nextProps.user.mapCenter
      })
    }
  }
  
  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value })
  };
  
  onSubmit = (e) => {
    e.preventDefault();
    
    const { name } = this.state;
    const coordinates = this.props.user.mapCenter.join(',');
    const pointData = { name, coordinates};
    
    this.props.addPoint(pointData);
    
    this.setState({
      name: '',
      errors: {}
    })
  };
  
  render() {
    return (
        <AddForm
          onSubmit={this.onSubmit}
          onInputChange={this.onChange}
          inputValue={this.state.name}
          errors={this.state.errors}
        />
    )
  }
}

AddForm.propTypes = {
  addPoint: propTypes.func
};

const mapStateToProps = ({ pointList: { points, errors }, auth: {  user } }) => {
  return { points, user, errors};
};

const mapDispatchToProps = (dispatch, {mapService}) => {
  return bindActionCreators({
    addPoint: addPoint(mapService)
  },dispatch)
};

export default compose(
    withMapService(),
    connect(mapStateToProps, mapDispatchToProps)
)(AddFormContainer)
