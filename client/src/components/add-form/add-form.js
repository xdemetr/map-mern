import React from 'react';
import propTypes from 'prop-types';
import TextField from "../form/text-field";

const AddForm = ({ onSubmit, onInputChange, inputValue, errors }) =>{
  return (
        <form className="form form_type_add" onSubmit={onSubmit}>
          <TextField
              placeholder="Название точки"
              onChange={onInputChange}
              value={inputValue}
              error={errors.name}
              name="name"
              autocomplete="off"
          />
        </form>
    );
};

AddForm.propTypes = {
  onSubmit: propTypes.func.isRequired,
  onInputChange: propTypes.func.isRequired,
  inputValue: propTypes.string.isRequired,
  errors: propTypes.object.isRequired
};

export default AddForm;
