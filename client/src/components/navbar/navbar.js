import React from 'react';
import propTypes from 'prop-types';
import { Link } from 'react-router-dom';
import AddForm from '../add-form';
import PointList from "../point-list";

const Navbar = ({isAuthenticated, onLogoutClick}) => {
  
  const authLinks = (
      <div className="dashboard">
        <div className="navigation__user-info user-info">
          <span
              className="user-info__button button_small button button_type_primary"
              onClick={onLogoutClick}>Выйти</span>
        </div>
        <AddForm/>
        <PointList/>
      </div>
  );
  
  const guestLinks = (
      <ul className="navigation__menu main-menu">
        <li className="main-menu__item">
          <Link to="/registration" className="main-menu__link link">Регистрация</Link>
        </li>
        <li className="main-menu__item">
          <Link to="/login" className="main-menu__link link">Вход</Link>
        </li>
      </ul>
  );
  
  return (
      <nav className="navigation">
        <Link to="/" className="navigation__logo">travel-route</Link>
        { isAuthenticated ? authLinks : guestLinks }
      </nav>
  );
};

Navbar.propTypes = {
  isAuthenticated: propTypes.bool.isRequired,
  onLogoutClick: propTypes.func.isRequired
};

export default Navbar;
