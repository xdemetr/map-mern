import React, {Component} from 'react';
import propTypes from 'prop-types';
import { connect } from "react-redux";
import { logoutUser } from '../../actions/auth-actions';
import { clearPoints } from '../../actions/point-actions';
import { bindActionCreators, compose } from "redux";
import { withMapService } from "../hoc-helpers";

import Navbar from './navbar';

class NavbarContainer extends Component {
  onLogoutClick = (e) => {
    e.preventDefault();
    this.props.logoutUser();
    this.props.clearPoints();
  };
  
  render() {
    const { isAuthenticated } = this.props;
    
    return (
        <Navbar
            isAuthenticated={isAuthenticated}
            onLogoutClick={this.onLogoutClick} />
    );
  }
}

Navbar.propTypes = {
  logoutUser: propTypes.func,
  clearPoints: propTypes.func
};

const mapStateToProps = ({ auth: { isAuthenticated, user }, pointList }  ) => {
  return { isAuthenticated, user, pointList };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    logoutUser: logoutUser,
    clearPoints: clearPoints
  },dispatch)
};

export default compose(
    withMapService(),
    connect(mapStateToProps, mapDispatchToProps)
)(NavbarContainer)
