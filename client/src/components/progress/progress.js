import React from 'react';
import './progress.sass';

const Progress = () => {
  return (
      <div className="progress">
        <div className="indeterminate"></div>
      </div>
  );
};

export default Progress;
