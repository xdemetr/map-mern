import React from 'react';
import { MapServiceConsumer } from '../map-service-context';

const withMapService = () => (Wrapped) => {
  return (props) => {
    return (
      <MapServiceConsumer>
        {
          (mapService) => {
            return <Wrapped {...props} mapService={mapService} />
          }
        }
      </MapServiceConsumer>
    );
  };
};

export default withMapService;
