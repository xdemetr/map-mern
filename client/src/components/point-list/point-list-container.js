import React, {Component} from 'react';
import propTypes from 'prop-types';
import { deletePoint, sortPoints, fetchPoints } from "../../actions/point-actions";
import './point-list.sass';
import Spinner from "../spinner";

import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { withMapService } from '../hoc-helpers';
import PointList from "./point-list";

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);
  return result;
};

class PointListContainer extends Component {
  state = {
    points: {}
  };
  
  componentDidMount() {
    this.props.fetchPoints(this.props.user.id);
  }
  
  componentWillReceiveProps(nextProps) {
    if (nextProps.points) {
      this.setState({
        points: nextProps.points
      });
    }
  }
  
  onDeleteClick = (id) => {
    this.props.deletePoint(id);
  };
  
  onDragEnd = (result) => {
    if (!result.destination) {
      return;
    }
    
    const points = reorder(
        this.state.points,
        result.source.index,
        result.destination.index
    );
    
    this.setState({points});
    this.props.sortPoints(points);
  };
  
  render() {
    const { points, loading } = this.props;
    
    if (!points) return;
    if (loading) return <Spinner/>;
    
    return (
        <PointList
            points={points}
            onDragEnd={this.onDragEnd}
            onDeleteClick={this.onDeleteClick}
        />
    )
  }
}

PointListContainer.propTypes = {
  fetchPoints: propTypes.func.isRequired,
  deletePoint: propTypes.func.isRequired,
  sortPoints: propTypes.func.isRequired
};

const mapStateToProps = ({ pointList: { points, loading }, auth: {  user } }) => {
  return { points, loading, user};
};

const mapDispatchToProps = (dispatch, {mapService}) => {
  return bindActionCreators({
    fetchPoints: fetchPoints(mapService),
    sortPoints: sortPoints(mapService),
    deletePoint: deletePoint(mapService)
  },dispatch)
};

export default compose(
    withMapService(),
    connect(mapStateToProps, mapDispatchToProps)
)(PointListContainer)
