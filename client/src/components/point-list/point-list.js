import React from 'react';
import propTypes from 'prop-types';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import './point-list.sass';

const PointList = ({ points, onDragEnd, onDeleteClick }) => {
  let items = '';
  
  if (Object.keys(points).length > 0) {
      items = points.map((point, idx) => (
          <Draggable key={point._id} draggableId={point._id} index={idx}>
            {(provided) => (
                <div
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    className="point-list__item">
                  <span className="point-list__name">
                    {point.name}
                  </span>
                  <span
                      onClick={() => onDeleteClick(point._id)}
                      className="point-list__button-delete button button_type_delete">
                    Удалить
                  </span>
                </div>
            )}
          </Draggable>
      ));
    }
    
  return(
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId="droppable">
          {(provided) => (
              <div
                  className="point-list"
                  ref={provided.innerRef}
                  {...provided.droppableProps}
              >
                {items}
                {provided.placeholder}
              </div>
          )}
        </Droppable>
      </DragDropContext>
  )
};

PointList.propTypes = {
  points: propTypes.oneOfType([propTypes.object, propTypes.array]),
  onDragEnd: propTypes.func.isRequired,
  onDeleteClick: propTypes.func.isRequired
};

export default PointList;
