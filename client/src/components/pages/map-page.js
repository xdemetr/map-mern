import React from 'react';
import AppMap from '../app-map';

const MapPage = () => {
  return (
      <div className="page page_type_map">
        <AppMap />
      </div>
  );
};

export default MapPage;
