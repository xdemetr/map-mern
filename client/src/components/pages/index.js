import LandingPage from './landing-page';
import RegistrationPage from './registration-page';
import LoginPage from "./login-page";
import MapPage from './map-page';

export {
  LandingPage,
  RegistrationPage,
  LoginPage,
  MapPage
}
