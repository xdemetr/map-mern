import React, {Component} from 'react';
import propTypes from 'prop-types';
import { withRouter, Link } from 'react-router-dom';
import { connect } from "react-redux";
import { registerUser } from "../../actions/auth-actions";
import TextField from "../form/text-field";
import { bindActionCreators, compose } from 'redux'
import {withMapService} from "../hoc-helpers";
import Progress from "../progress";

class RegistrationPage extends Component {
  state = {
    name: '',
    email: '',
    password: '',
    password2: '',
    errors: {}
  };
  
  componentDidMount() {
    if (this.props.isAuthenticated) {
      this.props.history.push('/map');
    }
  }
  
  componentWillReceiveProps(nextProps) {
    if(nextProps.errors) {
      this.setState({errors: nextProps.errors})
    }
  }
  
  onChange = (e) => {
    this.setState({[e.target.name]: e.target.value})
  };
  
  onSubmit = (e) => {
    e.preventDefault();
    const newUser = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
      password2: this.state.password2,
    };
  
    this.props.registerUser(newUser, this.props.history);
  };
  
  render() {
    const { errors } = this.state;
    const { loading } = this.props;
    const progress = loading ? <Progress/> : false;
    
    return (
        <div className="page page_type_auth">
          <div className="page__header">
            <h1 className="page__title">Регистрация</h1>
          </div>
          
          <div className="page__content">
            <form className="form form_type_auth" noValidate onSubmit={this.onSubmit}>
              {progress}
              <TextField
                  placeholder="Эл.почта"
                  type="email"
                  onChange={this.onChange}
                  value={this.state.email}
                  name="email"
                  error={errors.email}
              />
  
              <TextField
                  placeholder="Пароль"
                  type="password"
                  onChange={this.onChange}
                  value={this.state.password}
                  name="password"
                  error={errors.password}
              />
  
              <TextField
                  placeholder="Подтверждение пароля"
                  type="password"
                  onChange={this.onChange}
                  value={this.state.password2}
                  name="password2"
                  error={errors.password2}
              />
              
              <div className="form__actions">
                <button type="submit" className="form__button button button_type_primary">Зарегистрироваться</button>
                <Link to="/login" className="form__action-link link">У меня есть аккаунт</Link>
              </div>
            </form>
          </div>
        </div>
    );
  }
}

RegistrationPage.propTypes = {
  registerUser: propTypes.func.isRequired
};

const mapStateToProps = ({ auth: { isAuthenticated, loading, errors } }) => {
  return { isAuthenticated, loading, errors };
};

const mapDispatchToProps = (dispatch, {mapService}) => {
  return bindActionCreators({
    registerUser: registerUser(mapService)
  },dispatch)
};

export default compose(
    withMapService(),
    connect(mapStateToProps, mapDispatchToProps)
)(withRouter(RegistrationPage))
