import React, {Component} from 'react';
import propTypes from 'prop-types';
import {Link} from 'react-router-dom';
import { bindActionCreators, compose } from 'redux'
import { connect } from 'react-redux';
import { loginUser } from "../../actions/auth-actions";
import TextField from '../form/text-field';
import {withMapService} from "../hoc-helpers";
import Progress from "../progress";

class LoginPage extends Component {
  state = {
    email: '',
    password: '',
    errors: {}
  };
  
  componentDidMount() {
    if (this.props.isAuthenticated) {
      this.props.history.push('/map');
    }
  }
  
  componentWillReceiveProps(nextProps) {
    if (nextProps.isAuthenticated) {
      this.props.history.push('/map');
    }
    
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      })
    }
  }
  
  onChange = (e) => {
    this.setState({[e.target.name]: e.target.value})
  };
  
  onSubmit = (e) => {
    e.preventDefault();
    const userData = {
      email: this.state.email,
      password: this.state.password
    };
    this.props.loginUser(userData);
  };
  
  render() {
    const { errors } = this.state;
    const { loading } = this.props;
    const progress = loading ? <Progress/> : false;
    
    
    return (
        <div className="page page_type_auth">
          <div className="page__header">
            <h1 className="page__title">Авторизация</h1>
          </div>
      
          <div className="page__content">
            <form className="form form_type_auth" noValidate onSubmit={this.onSubmit}>
              {progress}
              
              <TextField
                placeholder="Эл.почта"
                type="email"
                onChange={this.onChange}
                value={this.state.email}
                name="email"
                error={errors.email}
              />
              
              <TextField
                placeholder="Пароль"
                type="password"
                onChange={this.onChange}
                value={this.state.password}
                name="password"
                error={errors.password}
              />
              
              <div className="form__actions">
                <button type="submit" className="button button_type_primary form__button">Войти</button>
                <Link to="/registration" className="form__action-link link">У меня нет аккаунта</Link>
              </div>
            </form>
          </div>
        </div>
    );
  }
}

LoginPage.propTypes = {
  loginUser: propTypes.func.isRequired
};

const mapStateToProps = ({ auth: { isAuthenticated, user, loading, errors } }  ) => {
  return { isAuthenticated, user, loading, errors };
};

const mapDispatchToProps = (dispatch, {mapService}) => {
  return bindActionCreators({
    loginUser: loginUser(mapService)
  },dispatch)
};

export default compose(
    withMapService(),
    connect(mapStateToProps, mapDispatchToProps)
)(LoginPage)
