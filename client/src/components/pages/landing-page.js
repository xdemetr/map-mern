import React from 'react';

const LandingPage = () => {
  return (
      <div className="page page_type_landing">
        <div className="page__header">
          <h1 className="page__title">Построй свой маршрут</h1>
        </div>
        <div className="page__content">
          <p>Создание машрутов доступно зарегистрированным пользователям.</p>
        </div>
      </div>
  );
};

export default LandingPage;
