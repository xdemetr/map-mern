import React from 'react';
import { Placemark } from "react-yandex-maps";

const PlacemarkList = ({points, onPointMoved}) => {
  if (!points) return null;
  
  const placemarkList = points.map((item) => {
    const coords = item.coordinates.split(',');
    
    return (
        <Placemark
            key={item._id}
            modules={['geoObject.addon.balloon']}
            geometry={[Number(coords[0]), Number(coords[1])]}
            properties={{
              balloonContentHeader: [item.name],
              balloonContentBody: [item.coordinates],
              iconContent: [item.order + 1]
            }}
            options={{
              draggable: true,
              preset: 'islands#darkGreenIcon'
            }}
            onDragend= {(event) => {
              onPointMoved(item._id, event.get('target').geometry.getCoordinates());
            }}
        />
    )
  });
  
  return placemarkList;
};

export default PlacemarkList;
