import React from 'react';
import { Polyline } from "react-yandex-maps";

const PolylineList = ({points}) => {
  if(!points) return null;
  
  const polylineCoords = points.map(item => {
    const currItem = item.coordinates.split(',');
  
    const intItem = currItem.map(item => {
      const crdItem = item.split(',');
      return Number(crdItem)
    });
    return intItem;
  });
  
  const polylineList = (
      <Polyline
          geometry= {polylineCoords}
          options= {{
            balloonCloseButton: false,
            strokeColor: '#4fa931',
            strokeWidth: 3,
            strokeOpacity: 0.8,
          }}
      />);
  
  return polylineList;
};

export default PolylineList;
