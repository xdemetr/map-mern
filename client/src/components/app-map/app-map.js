import React from 'react';
import propTypes from 'prop-types';
import classnames from 'classnames';
import { Map, YMaps } from "react-yandex-maps";
import PlacemarkList from './placemark-list';
import PolylineList from './polyline-list';

const AppMap =({ loading, points, mapCenter, onMapMoved, onPointMoved }) => {
  
  return (
      <YMaps>
        <Map
            className={ classnames('map', {'map_loading': loading}) }
            defaultState={{ center: mapCenter, zoom: 13 }}
            modules={['geocode']}
            onBoundsChange= {(event) => {
              onMapMoved(event.get('map').getCenter());
              //console.log(event.get('map').getCenter());
              event.get('map').container.fitToViewport();
            }}
            onSizeChange= {(event) => {
              event.get('map').container.fitToViewport();
              console.log(event);
              //console.log('hello', event.get('map').container.width());
            }}
        >
          <PlacemarkList points={points} onPointMoved={onPointMoved} />
          <PolylineList points={points} />
        </Map>
      </YMaps>
  );
};

AppMap.propTypes = {
  points: propTypes.oneOfType([propTypes.object, propTypes.array]),
  loading: propTypes.bool.isRequired,
  mapCenter: propTypes.arrayOf(propTypes.number)
};

export default AppMap;
