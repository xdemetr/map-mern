import React, {Component} from 'react';
import propTypes from 'prop-types';
import { connect } from "react-redux";
import { setMapCenter } from '../../actions/auth-actions';
import { fetchPoints, updatePoint } from '../../actions/point-actions';
import { bindActionCreators, compose } from 'redux';
import { withMapService } from "../hoc-helpers";
import AppMap from "./app-map";

class AppMapContainer extends Component {
  
  state = {
    points: {},
    loading: false
  };
  
  componentDidMount() {
    this.props.fetchPoints(this.props.user.id);
  }
  
  componentWillReceiveProps(nextProps) {
    if (nextProps.points) {
      this.setState({
        points: nextProps.points,
        loading: nextProps.loading
      });
    }
  }
  
  onMapMoved = (coords) => {
    this.props.setMapCenter(coords);
  };
  
  onPointMoved = (id, coordinates) => {
    const pointData = {
      coordinates: coordinates.join(',').toString()
    };
    this.props.updatePoint(id, pointData);
  };
  
  render() {
    
    const { points, loading } = this.state;
    const { mapCenter } = this.props.user;
    
    if (!points) return null;
    
    return (
        <AppMap
            loading={loading}
            points={points}
            mapCenter={mapCenter}
            onMapMoved={this.onMapMoved}
            onPointMoved={this.onPointMoved}
        />
    )
  }
}

AppMapContainer.propTypes = {
  fetchPoints: propTypes.func.isRequired,
  updatePoint: propTypes.func.isRequired,
  setMapCenter: propTypes.func.isRequired,
};

const mapStateToProps = ({ pointList: { points, loading }, auth: {  user } }) => {
  return { points, loading, user };
};

const mapDispatchToProps = (dispatch, {mapService}) => {
  return bindActionCreators({
    fetchPoints: fetchPoints(mapService),
    updatePoint: updatePoint(mapService),
    setMapCenter: setMapCenter(mapService)
  },dispatch)
};

export default compose(
    withMapService(),
    connect(mapStateToProps, mapDispatchToProps)
)(AppMapContainer)
