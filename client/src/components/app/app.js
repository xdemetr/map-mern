import React, {Component} from 'react';

import { Route, Switch } from 'react-router-dom'
import PrivateRoute from '../private-route';
import Navbar from "../navbar";
import {LandingPage, RegistrationPage, LoginPage, MapPage} from "../pages";


import { withMapService } from '../hoc-helpers';

import '../common';
import './app.sass';

class App extends Component {
  render() {
    return (
        <div className="app">
          <div className="app__body">
            <Route exact path="/" component={LandingPage} />
            <Route exact path="/registration" component={RegistrationPage}/>
            <Route exact path="/login" component={LoginPage}/>
            
            <Switch>
              <PrivateRoute exact path="/map" component={MapPage}/>
            </Switch>
          </div>
          <aside className="app__aside">
            <Navbar/>
          </aside>
        </div>
    );
  }
}

export default withMapService()(App);
