const validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validatePointInput(data) {
  let errors = {};
  
  data.name = !isEmpty(data.name) ? data.name : '';
  data.coordinates = !isEmpty(data.coordinates) ? data.coordinates : '';
  data.order = !isEmpty(data.order) ? data.order : '';
  
  if(!validator.isLength(data.name, {min: 2, max:20})) {
    errors.name = 'Название точки от 2 до 20 символов';
  }
  
  if(validator.isEmpty(data.coordinates)) {
    errors.name = 'Не указаны координаты';
  }
  
  if(!validator.isLatLong(data.coordinates)) {
    errors.coordinates = 'Координаты введены неверно';
  }
  
  if(validator.isEmpty(data.name)) {
    errors.name = 'Не указано название точки';
  }
  
  return {
    errors,
    isValid: isEmpty(errors)
  }
};
