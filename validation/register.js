const validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateRegisterInput(data) {
  let errors = {};
  
  data.email = !isEmpty(data.email) ? data.email : '';
  data.password = !isEmpty(data.password) ? data.password : '';
  data.password2 = !isEmpty(data.password2) ? data.password2 : '';
  
  if (validator.isEmpty(data.email)) {
    errors.email = 'Не указана эл.почта';
  }
  
  if (!validator.isEmail(data.email)) {
    errors.email = 'Адрес эл.почты введен неверно';
  }
  
  if (validator.isEmpty(data.password)) {
    errors.password = 'Не указан пароль';
  }
  
  if (!validator.isLength(data.password, { min: 6, max: 30 })) {
    errors.password = 'Пароль должен быть от 6 символов';
  }
  
  if (validator.isEmpty(data.password2)) {
    errors.password2 = 'Не указано подтверждение пароля';
  } else {
    if (!validator.equals(data.password, data.password2)) {
      errors.password2 = 'Пароли должны совпадать';
    }
  }
  
  return {
    errors,
    isValid: isEmpty(errors)
  }
};
