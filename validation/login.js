const validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateLoginInput(data) {
  let errors = {};
  
  data.email = !isEmpty(data.email) ? data.email : '';
  data.password = !isEmpty(data.password) ? data.password : '';
  
  if(!validator.isEmail(data.email)) {
    errors.email = 'Адрес эл.почты введен неверно';
  }
  
  if(validator.isEmpty(data.email)) {
    errors.email = 'Не указана эл.почта';
  }
  
  if(validator.isEmpty(data.password)) {
    errors.password = 'Не указан пароль';
  }
  
  return {
    errors,
    isValid: isEmpty(errors)
  }
};
