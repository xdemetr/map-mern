const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const keys = require('../../config/keys');
const passport = require('passport');

const validateRegisterInput = require('../../validation/register');
const validateLoginInput = require('../../validation/login');

const User = require('../../models/user');

// @route   POST api/users/register
// @desc    Регистрация
// @access  Public
router.post('/register', (req, res) => {
  const { errors, isValid } = validateRegisterInput(req.body);
  
  if (!isValid) {
    return res.status(400).json(errors)
  }
  
  User.findOne({ email: req.body.email })
  .then(user => {
    if (user) {
      errors.email = 'Пользователь с таким email уже зарегистрирован';
      return res.status(400).json(errors)
    } else {
      const newUser = new User({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        mapCenter: keys.mapCenter
      });
      
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) throw new err;
          newUser.password = hash;
          newUser.save()
          .then(user => res.json(user))
          .catch(err => console.log(err))
        })
      })
    }
  })
});

// @route   POST api/users/login
// @desc    Авторизация
// @access  Public
router.post('/login', (req, res) => {
  const { errors, isValid } = validateLoginInput(req.body);
  
  if (!isValid) {
    return res.status(400).json(errors)
  }
  
  const email = req.body.email;
  const password = req.body.password;
  
  User.findOne({email})
  .then(user => {
    if (!user) {
      errors.email = 'Пользователь не найден';
      return res.status(404).json(errors)
    }
    
    bcrypt.compare(password, user.password)
    .then(isMatch => {
      if (isMatch) {
        const payload = {id: user.id, email: user.email, mapCenter: user.mapCenter};
        
        // Sign token
        jwt.sign(
            payload,
            keys.secretOrKey,
            {expiresIn: 3600},
            (err, token) => {
              res.json({
                success: true,
                token: 'Bearer ' + token
              })
            });
      } else {
        errors.password= 'Неверный пароль';
        return res.status(400).json(errors)
      }
    })
  })
});

// @route   PUT api/users
// @desc    Редактирование
// @access  Private
router.put(
    '/',
    passport.authenticate('jwt', {session: false}), (req, res) => {
      User.findByIdAndUpdate(
          req.user.id,
          {mapCenter: req.body}
      )
      .then(user => {
        user.save().then(user => res.json(user));
      })
      .catch(err => console.log(err));
    });

module.exports = router;
