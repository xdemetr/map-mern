const express = require('express');
const router = express.Router();
const passport = require('passport');
const validatePointInput = require('../../validation/point');

const Point = require('../../models/point');

// @route   GET api/points
// @desc    Список точек пользователя
// @access  Private
router.get(
  '/',
  passport.authenticate('jwt', {session: false}),
  (req, res) => {
    Point.find({user: req.user.id})
    .sort({order: 1})
    .then(points => res.json(points))
    .catch(err => res.status(404).json({nopoints: 'Нет точек'}))
  }
);

// @route   POST api/points
// @desc    Добавление
// @access  Private
router.post(
  '/',
  passport.authenticate('jwt', {session: false}),
  (req, res) => {
    const {errors, isValid} = validatePointInput(req.body);
    
    if(!isValid) {
      return res.status(400).json(errors)
    }
  
    Point.find({user: req.user.id})
    .then(points => {
      const order = Object.keys(points).length;
      
      const newPoint = new Point ({
        name: req.body.name,
        coordinates: req.body.coordinates,
        user: req.user.id,
        order
      });
  
      newPoint.save()
      .then(() => (
          Point.find({user: req.user.id})
          .sort({order: 1})
          .then(points => res.json(points))
      ));
    });
});

// @route   PUT api/points/:id
// @desc    Редактирование
// @access  Private
router.put(
    '/:id',
    passport.authenticate('jwt', {session: false}), (req, res) => {
      
      const pointFields = {};
      pointFields.user = req.user.id;
      
      if (req.body.coordinates) pointFields.coordinates = req.body.coordinates;
      if (req.body.order) pointFields.order = req.body.order;
      
      
      Point.findByIdAndUpdate(
          req.params.id,
          pointFields
          ).then(point => {
            point.save().then(
                Point.find({user: req.user.id})
                .sort({order: 1})
                .then(points => res.json(points))
            );
      });
    });

// @route   DELETE api/points/:id
// @desc    Удаление
// @access  Private
router.delete(
    '/:id',
    passport.authenticate('jwt', {session: false}), (req, res) => {
      Point.findOneAndRemove({_id: req.params.id})
      .then(point => {
        if(!point){
          return res.status(404).json({notfound: 'Точка не найдена'})
        }
        Point.find({user: req.user.id})
        .sort({order: 1})
        .then(points => res.json(points))
      });
    });

// @route   PUT api/points
// @desc    Изменение сортировки
// @access  Private
router.put(
    '/',
    passport.authenticate('jwt', {session: false}), (req, res) => {
      const points = req.body;
      points.map((point, idx) => {
        Point.findByIdAndUpdate(
            point._id,
            { order: idx}
        )
        .then(pp => {
          pp.save().then(
              Point.find({user: req.user.id})
              .sort({order: 1})
              .then(points => res.json(points))
          )
        })
        .catch(err => console.log(err));
      });
    });

module.exports = router;
